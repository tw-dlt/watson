#!/usr/local/bin/python3

print("Going to decode some data...")

rawData = "0x00000000000000000000000000000000000000000000000000000000000000200000000000000000000000000000000000000000000000000000000000000004000000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000000000000000000000000000000000000c000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000000140000000000000000000000000000000000000000000000000000000000000000a4672656967687465723100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000a4672656967687465723200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000a4672656967687465723300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000a4672656967687465723400000000000000000000000000000000000000000000"

data = rawData[2:len(rawData)]

print("length of data: ", len(data))

def trim(input_string, start, count):
    return input_string[start:start+count]


def expand_to_hex_chars(byte_count):
    return byte_count <<1


def read_uint_value(hex_string):
    return int("0x" + hex_string, 16)


def parse_hex_string(hex_string):
    return bytearray.fromhex(hex_string).decode()




def read_int(data, start, count, expand_to_hex=False):
    hex_data = trim(data, start, count)
    int_value = read_uint_value(hex_data)
    if expand_to_hex:
        print("%.4d : %s -> %.4d -> %.4d" % (start, hex_data, int_value, expand_to_hex_chars(int_value)))
    else:
        print("%.4d : %s -> %.4d" % (start, hex_data, int_value))
    return int_value

def read_string(data, start, count):
    hex_data = trim(data, start, count)
    parsed_string = parse_hex_string(hex_data)
    print("%.4d : %64s -> %s" % (start, hex_data, parsed_string))
    return parsed_string

def read_element(data, start, offset):
    length_of_element = read_int(data, start, offset, True) <<1
    return read_string(data, start+offset, length_of_element)



offset = expand_to_hex_chars(read_int(data, 0, 64, True))

current_offset = offset

length_of_array = read_int(data, current_offset, offset)


current_offset += offset
data_start_offset = current_offset

element_offsets = []

for i in range(0, length_of_array):
    element_offset = expand_to_hex_chars(read_int(data, current_offset, offset, True))
    current_offset += offset
    element_offsets.append(element_offset)


element_values = []


for i in range(0, length_of_array):
    element_value = read_element(data, data_start_offset + element_offsets[i], offset)
    element_values.append(element_value)



print("Parsed Array:")
for i in range(0, length_of_array):
    e = element_values[i]
    print("[",i, "] ", e)







package thoughtworks.dlt.watson;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;
import org.web3j.quorum.Quorum;
import thoughtworks.dlt.watson.quorum.QuorumApi;
import thoughtworks.dlt.watson.quorum.QuorumApiCache;

import java.net.MalformedURLException;
import java.net.URL;

public class JavalinContext {
    public static URL getNodeUrlFromRequest(Context ctx) {
        String urlParameter = ctx.queryParam("nodeUrl");
        try {
            return new URL(urlParameter);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Could not parse url : " + urlParameter, e);
        }
    }

    public static void setQuorumApiCacheTo(Context ctx, QuorumApiCache quorumApiCache) {
        ctx.attribute("quorumApiCache", quorumApiCache);
    }

    public static Quorum getQuorumApiFrom(Context ctx) {
        URL nodeUrl = getNodeUrlFromRequest(ctx);
        QuorumApiCache quorumApiCache = ctx.attribute("quorumApiCache");

        return quorumApiCache.getInstance(nodeUrl);
    }

    private static QuorumApiCache QUORUM_API_CACHE = new QuorumApiCache(new QuorumApi());
    public static Handler injectQuorumApiCache = ctx -> {
        setQuorumApiCacheTo(ctx, QUORUM_API_CACHE);
    };


}

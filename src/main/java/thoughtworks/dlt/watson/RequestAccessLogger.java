package thoughtworks.dlt.watson;

import io.javalin.http.Context;
import io.javalin.http.RequestLogger;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestAccessLogger implements RequestLogger {

    private final RequestLogFormat logFormat = new StandardFormat();

    private static final Logger log = LoggerFactory.getLogger(RequestAccessLogger.class.getName());
    @Override
    public void handle(@NotNull Context ctx, @NotNull Float executionTimeMs) throws Exception {
        log.info(logFormat.format(executionTimeMs, ctx.req, ctx.res));
    }

    interface RequestLogFormat {
        String format(Float executionTimeMs, HttpServletRequest request, HttpServletResponse response);
    }

    static class StandardFormat implements RequestLogFormat {

        @Override
        public String format(Float executionTimeMs,
                             HttpServletRequest request,
                             HttpServletResponse response) {

            StringBuilder sb = new StringBuilder();
            sb.append(request.getMethod()).append(" ")
                    .append(request.getPathInfo())
                    .append((request.getQueryString() != null) ? "?" + request.getQueryString() : "").append(" ")
                    .append(response.getStatus()).append(" ")
                    .append(String.format("%.0f", executionTimeMs)).append("ms");
            return sb.toString();
        }
    }
}

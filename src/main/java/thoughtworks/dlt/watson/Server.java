package thoughtworks.dlt.watson;

import io.javalin.Javalin;
import thoughtworks.dlt.watson.quorum.QuorumApi;
import thoughtworks.dlt.watson.quorum.QuorumApiCache;

import static thoughtworks.dlt.watson.JavalinContext.injectQuorumApiCache;
import static thoughtworks.dlt.watson.blocks.BlocksEndpoint.serveBlocksIndex;
import static thoughtworks.dlt.watson.nodestatus.NodeStatusEndpoint.serveNodeStatusIndex;

public class Server {

    public static void main(String[] args) {
        Javalin api = Javalin.create().start(7000);

        api.config.requestLogger(new RequestAccessLogger());

        api.before("*", injectQuorumApiCache);

        api.get("/", ctx -> ctx.result("Index"));
        api.get("/blocks", serveBlocksIndex);
        api.get("/nodestatus", serveNodeStatusIndex);

    }
}

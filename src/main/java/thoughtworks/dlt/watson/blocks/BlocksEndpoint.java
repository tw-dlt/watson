package thoughtworks.dlt.watson.blocks;

import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.Transaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.quorum.Quorum;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static thoughtworks.dlt.watson.JavalinContext.getQuorumApiFrom;

public class BlocksEndpoint {

    private static final Logger log = LoggerFactory.getLogger(BlocksEndpoint.class);

    public static Handler serveBlocksIndex = ctx -> {
        ctx.json(loadBlocks(getQuorumApiFrom(ctx)));
    };

    /**
     * Load the lasrt 10 blocks
     * @todo implement paging
     */
    private static List<WatsonBlock> loadBlocks(Quorum quorumApi) {
        try {
            BigInteger blockNumber = quorumApi.ethBlockNumber().send().getBlockNumber();

            ArrayList<WatsonBlock> watsonBlocks = new ArrayList<>();

            for (int i=0; i<10; ++i) {

                BigInteger currentBlock = blockNumber.subtract(new BigInteger("" + i, 10));
                if (lessThanZero(currentBlock)) {
                    break;
                }
                watsonBlocks.add(loadBlock(quorumApi, i, currentBlock));
            }
            return watsonBlocks;

        } catch (Exception e) {
            throw new RuntimeException("Could not load blocks", e);
        }
    }

    @NotNull
    private static WatsonBlock loadBlock(Quorum quorumApi, int i, BigInteger blockNumber) {
        try {
            EthBlock.Block block = quorumApi.ethGetBlockByNumber(DefaultBlockParameter.valueOf(blockNumber), true).send().getBlock();

            List<EthBlock.TransactionResult> ethTxs = block.getTransactions();

            List<WatsonTransaction> txs = new ArrayList<>();
            for (EthBlock.TransactionResult<EthBlock.TransactionObject> ethTx : ethTxs) {
                Transaction txObject = ethTx.get();
                Optional<TransactionReceipt> txReceipt = quorumApi.ethGetTransactionReceipt(txObject.getHash()).send().getTransactionReceipt();

                WatsonTransaction tx = new WatsonTransaction(txObject, txReceipt);
                txs.add(tx);
            }




            return new WatsonBlock(block.getNumber(), block.getHash(), txs);
        } catch (Exception e) {
            throw new RuntimeException("Could not load block " + blockNumber, e);
        }
    }

    private static boolean lessThanZero(BigInteger blockNumber) {
        return blockNumber.compareTo(new BigInteger("0", 10)) == -1;
    }


}

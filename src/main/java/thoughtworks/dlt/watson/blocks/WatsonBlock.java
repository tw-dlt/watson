package thoughtworks.dlt.watson.blocks;

import java.math.BigInteger;
import java.util.List;

public class WatsonBlock {

    public final BigInteger blockNumber;
    public final String blockHash;
    public List<WatsonTransaction> txs;

    public WatsonBlock(BigInteger blockNumber, String blockHash, List<WatsonTransaction> txs) {
        this.blockNumber = blockNumber;
        this.blockHash = blockHash;
        this.txs = txs;
    }
}

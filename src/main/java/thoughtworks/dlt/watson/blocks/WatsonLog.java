package thoughtworks.dlt.watson.blocks;

import org.jetbrains.annotations.NotNull;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.*;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.protocol.core.methods.response.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.checkedCollection;
import static java.util.Collections.emptyList;

/**
 * http://ethdocs.org/en/latest/contracts-and-transactions/accessing-contracts-and-transactions.html
 * <p>
 * https://solidity.readthedocs.io/en/develop/abi-spec.html
 */
public class WatsonLog {

    /**
     * Data is hex encoded value with some padding.
     */
    public final String data;
    public final String type;
    public final List<String> dataDecoded;

    /**
     * * The topic is the sha3 hash of the signature of the event, e.g :
     * * > web3.sha3("Print(uint256)")
     *
     * * "24abdb5865df5079dcc5ac590ff6f01d5c16edbc5fab4e195d9febd1114503da"
     * *
     */
    public final List<String> topics;

    public WatsonLog(Log ethLog) {
        data = ethLog.getData();
        type = ethLog.getType();
        topics = ethLog.getTopics();
        dataDecoded = Collections.emptyList(); //decodeData(data); //
    }



    private static List<String> decodeData(String data) {
        if (data.length() == 0) {
            return emptyList();
        }

//        List<TypeReference<Type>> outputParameters = arrayOfUtf8Strings();

//        List<TypeReference<Type>> outputParameters = staticArrayOfUtf8Strings();

        List<TypeReference<Type>> outputParameters = dynamicArrayOfUtf8String();

        List<String> decoded = new ArrayList<>();

        List<Type> resultParameters = FunctionReturnDecoder.decode(data, outputParameters);

        List<Utf8String> utf8Strings = (List<Utf8String>) resultParameters.get(0).getValue();
        for (Utf8String utf8String : utf8Strings) {
            decoded.add(utf8String.getValue());
        }

        return decoded;
    }

    private static List<TypeReference<Type>> dynamicArrayOfUtf8String() {
        Event e = new Event("InitializeFreighters",
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Utf8String>>() {}));
        return e.getNonIndexedParameters();
    }

    @NotNull
    private static List<TypeReference<Type>> staticArrayOfUtf8Strings() {
        List<TypeReference<Type>> outputParameters = new ArrayList<>(1);
        outputParameters.add((TypeReference)new TypeReference.StaticArrayTypeReference<StaticArray<Utf8String>>(4) {});
        return outputParameters;
    }


    private static List<TypeReference<Type>> arrayOfUtf8Strings() {
        Function function = new Function("InitializeFreighters",
                Arrays.asList(),
                Collections.singletonList(new TypeReference<Utf8String>() {
                }));
        return function.getOutputParameters();
    }
}

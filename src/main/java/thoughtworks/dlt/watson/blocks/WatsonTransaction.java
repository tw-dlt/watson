package thoughtworks.dlt.watson.blocks;

import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.Transaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class WatsonTransaction {
    public final String input;
    public final String raw;
    public final String hash;
    public final String contractAddress;
    public List<WatsonLog> logs = new ArrayList<>();

    public WatsonTransaction(Transaction txObject, Optional<TransactionReceipt> txReceiptOpt) {
        hash = txObject.getHash();
        input = txObject.getInput();
        raw = txObject.getRaw();

        if(txReceiptOpt.isPresent()) {
            TransactionReceipt txReceipt = txReceiptOpt.get();
            List<Log> ethLogs = txReceipt.getLogs();

            contractAddress = txReceipt.getContractAddress();

            for (Log ethLog : ethLogs) {
                WatsonLog log = new WatsonLog(ethLog);
                logs.add(log);
            }

        } else {
            contractAddress = "";
        }
    }
}

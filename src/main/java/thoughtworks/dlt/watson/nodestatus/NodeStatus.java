package thoughtworks.dlt.watson.nodestatus;

public class NodeStatus {

    public final String status;
    public final String message;

    public NodeStatus(String status, String message) {
        this.status = status;
        this.message = message;
    }
}

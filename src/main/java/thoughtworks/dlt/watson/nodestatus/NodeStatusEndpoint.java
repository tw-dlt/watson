package thoughtworks.dlt.watson.nodestatus;

import io.javalin.http.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import thoughtworks.dlt.watson.blocks.BlocksEndpoint;
import thoughtworks.platform.http.Http;

import java.net.URL;

import static thoughtworks.dlt.watson.JavalinContext.getNodeUrlFromRequest;
import static thoughtworks.platform.http.Http.POST;

public class NodeStatusEndpoint {

    private static final Logger log = LoggerFactory.getLogger(BlocksEndpoint.class);

    // This is the POST request you can send to a geth node to know if its running.
    public static final String STATUS_BODY = "{\"jsonrpc\":\"2.0\",\"method\":\"web3_clientVersion\",\"params\":[],\"id\":67}";

    public static Handler serveNodeStatusIndex = ctx -> {
        URL nodeUrl = getNodeUrlFromRequest(ctx);

        NodeStatus nodeStatus = null;

        try {
            Http.Response response = POST(nodeUrl, "application/json", STATUS_BODY);

            int statusCode = response.statusCode;

            String status = (statusCode == 200) ? "up" : "down";

            final String message;

            if (statusCode != 200) {
                message = "Node is not happy: Status " + statusCode + " response: " + response.content;
            } else {
                message = response.content;
            }
            nodeStatus = new NodeStatus(status, message);
        } catch (Exception e) {
            nodeStatus = new NodeStatus("down", nodeUrl + " " + e.getMessage());
        }
        ctx.json(nodeStatus);
    };

}

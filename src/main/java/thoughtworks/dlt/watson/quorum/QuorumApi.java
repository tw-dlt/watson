package thoughtworks.dlt.watson.quorum;

import org.web3j.protocol.Web3jService;
import org.web3j.protocol.http.HttpService;
import org.web3j.quorum.Quorum;

import java.net.URL;

public class QuorumApi {


    public Quorum connectTo(URL nodeUrl) {
        return Quorum.build(new HttpService(nodeUrl.toString()));
    }
}

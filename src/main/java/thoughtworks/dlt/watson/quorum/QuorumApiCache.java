package thoughtworks.dlt.watson.quorum;

import org.web3j.quorum.Quorum;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class QuorumApiCache {

    private final QuorumApi quorumApi;

    private final Map<URL, Quorum> quorumMap = new HashMap<>();



    public QuorumApiCache(QuorumApi quorumApi) {
        this.quorumApi = quorumApi;
    }

    public Quorum getInstance(URL nodeUrl) {
        if (!quorumMap.containsKey(nodeUrl)) {
            addNewInstance(nodeUrl);
        }
        return quorumMap.get(nodeUrl);
    }

    synchronized private void addNewInstance(URL nodeUrl) {
        Quorum quorum = quorumApi.connectTo(nodeUrl);
        quorumMap.put(nodeUrl, quorum);
    }
}

package thoughtworks.platform.http;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import thoughtworks.platform.json.JsonParsing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.EMPTY_LIST;

public class Http {

    private static final Logger log = LoggerFactory.getLogger(Http.class);

    private final HeaderBuilder headerBuilder = new HeaderBuilder();

    public static Response GET(URL url) {
        return GET(url, EMPTY_LIST);
    }

    public static Response GET(URL url, List<Header> headers) {
        try {
            log.debug("GET " + url + " HTTP/1.1");
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(url.toURI());

            for (Header header : headers) {
                request.addHeader(header);
            }

            return executeMethod(client.execute(request));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Response POST(URL url, String contentType, String body) {
        try {
            log.debug("POST " + url + " HTTP/1.1");
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(url.toURI());
            request.setEntity(new StringEntity(body));
            request.setHeader("Accept", contentType);
            request.setHeader("Content-Type", contentType);

            return executeMethod(client.execute(request));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @NotNull
    private static Response executeMethod(HttpResponse execute) throws IOException {
        HttpResponse response = execute;

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }


        int statusCode = response.getStatusLine().getStatusCode();
        String reasonPhrase = response.getStatusLine().getReasonPhrase();
        String content = result.toString();

        log.debug("HTTP/1.1 " + statusCode + " " + reasonPhrase);


        if (response.getEntity().getContentType().getValue().contains("application/json")) {
            log.debug(JsonParsing.prettyPrintJson(content));
        } else {
            log.debug(content);
        }

        return new Response(statusCode,
                reasonPhrase,
                content,
                response);
    }

    public static class Response {
        public final int statusCode;
        public final String statusText;
        public final String content;
        public final HttpResponse original;

        public Response(int statusCode, String statusText, String content, HttpResponse original) {
            this.statusCode = statusCode;
            this.statusText = statusText;
            this.content = content;
            this.original = original;
        }
    }

    public static HeaderBuilder headers() {
        return new HeaderBuilder();
    }

    public static class HeaderBuilder {
        List<Header> headers = new ArrayList<>();

        public HeaderBuilder add(String name, String value) {
            headers.add(new BasicHeader(name, value));
            return this;
        }

        public List<Header> build() {
            return headers;
        }
    }

}

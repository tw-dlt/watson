package thoughtworks.platform.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;

public class JsonParsing {


    public static String prettyPrintJson(String jsonString) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Object json = mapper.readValue(jsonString, Object.class);

            String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);

            return indented;

        } catch (Exception e) {
            throw new RuntimeException("could not pars json" + jsonString, e);
        }
    }

    public static Object parseJsonString(String jsonString) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(jsonString, Object.class);
        } catch (Exception e) {
            throw new RuntimeException("could not pars json" + jsonString, e);
        }
    }


    public static Object getItemFromJson(List<Object> json, String fieldName, int index) {
        return map(json.get(index)).get(fieldName);
    }

    private static Map<Object, Object> map(Object o) {
        return (Map<Object, Object>)o;
    }
}

package thoughtworks.dlt.watson;


import org.junit.Test;
import thoughtworks.platform.test.StubHttpCall;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static thoughtworks.platform.test.StubHttpCall.stubHttpRequest;

public class StandardRequestAccessLoggerTest {

    @Test
    public void test_standard_output_format() {

        RequestAccessLogger.StandardFormat format = new RequestAccessLogger.StandardFormat();

        StubHttpCall stubHttpCall = stubHttpRequest()
                .GET("/some/path").withQueryString("foo=bar")
                .returns().Ok()
                .build();



        String output = format.format(1.2f, stubHttpCall.servletRequest(), stubHttpCall.servletResponse());

        assertThat(output, is("GET /some/path?foo=bar 200 1ms"));
    }

    @Test
    public void test_with_no_query_string() {
        RequestAccessLogger.StandardFormat format = new RequestAccessLogger.StandardFormat();

        StubHttpCall stubHttpCall = stubHttpRequest()
                .GET("/some/path")
                .returns().Ok()
                .build();



        String output = format.format(1.2f, stubHttpCall.servletRequest(), stubHttpCall.servletResponse());

        assertThat(output, is("GET /some/path 200 1ms"));

    }
}

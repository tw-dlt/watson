package thoughtworks.dlt.watson;

import org.jetbrains.annotations.NotNull;
import thoughtworks.platform.http.Http;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class WatsonApi {
    @NotNull
    public static Http.Response GET(String endpoint, String nodeUrlString) throws UnsupportedEncodingException, MalformedURLException {
        String encodedParameter = URLEncoder.encode(nodeUrlString, "UTF8");


        URL url = new URL("http://localhost:7000/" + endpoint + "?nodeUrl=" + encodedParameter);

        return Http.GET(url);
    }
}

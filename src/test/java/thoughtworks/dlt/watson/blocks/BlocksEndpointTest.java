package thoughtworks.dlt.watson.blocks;

import org.junit.Before;
import org.junit.Test;
import org.web3j.crypto.Hash;
import org.web3j.utils.Numeric;
import thoughtworks.dlt.watson.quorum.StubQuorumApi;
import thoughtworks.platform.test.StubHttpServletRequest;
import thoughtworks.platform.test.StubJavalinContext;
import thoughtworks.platform.test.StubQuorum;

import java.math.BigInteger;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static thoughtworks.dlt.watson.blocks.BlocksEndpoint.serveBlocksIndex;
import static thoughtworks.platform.json.JsonParsing.getItemFromJson;
import static thoughtworks.platform.json.JsonParsing.parseJsonString;
import static thoughtworks.platform.test.StubHttpCall.stubHttpRequest;
import static thoughtworks.platform.test.StubJavalinContext.stubJavalinContext;

public class BlocksEndpointTest {

    StubQuorumApi stubQuorumApi = new StubQuorumApi();
    private StubQuorum stubQuorum = stubQuorumApi.stubQuorum;
    private StubHttpServletRequest stubRequest;
    private StubJavalinContext ctx;

    @Before
    public void setup_stubs() {
        stubRequest = stubHttpRequest()
                .withQueryString("nodeUrl=http://localhos:666")
                .servletRequest();

        ctx = stubJavalinContext()
                .withRequest(stubRequest)
                .withQuorumApi(stubQuorumApi)
                .build();

    }

    @Test
    public void loads_blocks() throws Exception {
        stubQuorum.isAtBlockNumber(100);

        serveBlocksIndex.handle(ctx);
        List<Object> json = getJsonResponse(ctx);

        assertThat(json.size(), is(10));
        assertThat(getItemFromJson(json, "blockNumber", 0), is(100));
        assertThat(getItemFromJson(json, "blockHash", 0), is("foobaaa0x64"));
        assertThat(getItemFromJson(json, "blockNumber", 9), is(91));
        assertThat(getItemFromJson(json, "blockHash", 9), is("foobaaa0x5b"));
    }

    private static List<Object> getJsonResponse(StubJavalinContext ctx) {
        String jsonString = ctx.resultString();

        return (List<Object>) parseJsonString(jsonString);
    }


    @Test
    public void doesnt_load_more_blocks_if_at_block_0() throws Exception {
        stubQuorum.isAtBlockNumber(0);

        serveBlocksIndex.handle(ctx);
        List<Object> json = getJsonResponse(ctx);

        assertThat(json.size(), is(1));
        assertThat(getItemFromJson(json, "blockNumber", 0), is(0));
        assertThat(getItemFromJson(json, "blockHash", 0), is("foobaaa0x0"));

    }


    @Test
    public void biginteger_as_hex() {

        BigInteger input = new BigInteger("100", 10);
        BigInteger result = Numeric.decodeQuantity("0x" + input.toString(16));

        assertThat(input, is(result));

    }

    @Test
    public void compare_big_integers() {
        BigInteger input = new BigInteger("-1", 10);
        BigInteger test = new BigInteger("0", 10);

        assertThat(input.compareTo(test), is(-1));

    }

    @Test
    public void sha3_of_event_name() {
        String eventSig = "Print(uint256)";

        String hash = Hash.sha3String(eventSig);

        assertThat(hash, is("0x24abdb5865df5079dcc5ac590ff6f01d5c16edbc5fab4e195d9febd1114503da"));
    }

    @Test
    public void sha3_of_initializee_freighters() {
        String eventSig = "InitializeFreighters(string[])";
        String hash = Hash.sha3String(eventSig);

        assertThat(hash, is("0xb68be16c32c32758195dd6cfc529cd00417cd98343aa09c6f94d46a7c86d4254"));
    }


}

package thoughtworks.dlt.watson.blocks;

import org.junit.Test;
import thoughtworks.dlt.watson.WatsonApi;
import thoughtworks.platform.http.Http;
import thoughtworks.platform.json.JsonParsing;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BlocksIntegrationTest {


    @Test
    public void returns_blocks_from_node() throws Exception {
        Http.Response response = WatsonApi.GET("blocks", "http://localhost:22002");

        System.out.println(JsonParsing.prettyPrintJson(response.content));
        assertThat(response.statusCode, is(200));

    }
}

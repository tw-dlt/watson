package thoughtworks.dlt.watson.nodestatus;

import org.junit.Ignore;
import org.junit.Test;
import thoughtworks.dlt.watson.WatsonApi;
import thoughtworks.platform.http.Http;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class NodeStatusIntegrationTest {

    @Ignore("Needs to be moved into int testing, also need a fake quorum?")
    @Test
    public void confirms_status_of_a_quorum_node() throws Exception {


        Http.Response response = WatsonApi.GET("nodestatus", "http://localhost:22003");

        assertThat(response.statusCode, is(200));

        System.out.println(response.content);
    }

}

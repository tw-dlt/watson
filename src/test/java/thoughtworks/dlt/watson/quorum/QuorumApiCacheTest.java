package thoughtworks.dlt.watson.quorum;

import org.junit.Test;
import org.web3j.quorum.Quorum;

import java.net.URL;

import static org.junit.Assert.assertNotNull;

public class QuorumApiCacheTest {

    @Test
    public void connects_and_gets_an_instance() throws Exception {

        QuorumApiCache cache = new QuorumApiCache(new StubQuorumApi());

        URL nodeUrl = new URL("http://localhost:3344");

        Quorum quorumApi = cache.getInstance(nodeUrl);

        assertNotNull("QuorumApi should not be null", quorumApi);
    }


}

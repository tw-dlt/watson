package thoughtworks.dlt.watson.quorum;

import org.junit.Test;
import org.web3j.protocol.core.methods.response.EthProtocolVersion;
import org.web3j.protocol.core.methods.response.NetListening;
import org.web3j.protocol.core.methods.response.NetVersion;
import org.web3j.quorum.Quorum;

import java.net.URL;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class QuorumApiIntegrationTest {

    @Test
    public void connects_to_a_quorum_instance() throws Exception {
        QuorumApi api = new QuorumApi();

        Quorum quorum = api.connectTo(new URL("http://localhost:22003"));

        assertNotNull("Should not return null", quorum);

        NetListening netListening = quorum.netListening().send();
        assertThat("Should be listening", netListening.isListening(), is(true));
    }
}

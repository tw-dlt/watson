package thoughtworks.dlt.watson.quorum;

import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.Response;
import org.web3j.protocol.core.methods.response.EthBlockNumber;

import java.io.IOException;
import java.math.BigInteger;

public class StubGethRequest<T extends Response> extends Request<String, T> {


    private T response;

    public StubGethRequest(T response) {
        this.response = response;
    }

    @Override
    public T send() throws IOException {
        return response;
    }
}

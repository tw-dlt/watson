package thoughtworks.dlt.watson.quorum;

import org.web3j.quorum.Quorum;
import thoughtworks.platform.test.StubQuorum;

import javax.rmi.CORBA.Stub;
import java.net.URL;

public class StubQuorumApi extends QuorumApi {

    public final StubQuorum stubQuorum = new StubQuorum();
    @Override
    public Quorum connectTo(URL nodeUrl) {
        return stubQuorum;
    }


}

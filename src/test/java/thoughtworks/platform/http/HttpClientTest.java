package thoughtworks.platform.http;

import org.junit.Ignore;
import org.junit.Test;

import java.net.URL;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static thoughtworks.platform.http.Http.GET;
import static thoughtworks.platform.http.Http.POST;

public class HttpClientTest {

    // @todo run a test webserver to call
    @Test
    public void gets_a_url() throws Exception {
        URL url = new URL("https://www.google.com");
        Http.Response response = GET(url);

        assertThat(response.statusCode, is(200));
        assertThat(response.content, startsWith("<!doctype"));
    }


    @Test
    @Ignore("only runs when server is running")
    public void post_some_json() throws Exception {
        URL url = new URL("http://localhost:22003");
        Http.Response response = POST(url, "application/json", "{\"jsonrpc\":\"2.0\",\"method\":\"web3_clientVersion\",\"params\":[],\"id\":67}");

        assertThat(response.statusCode, is(200));
        assertThat(response.content, startsWith("{\"jsonrpc\":\"2.0\",\"id\":67,\"result\":\"Geth"));
    }
}

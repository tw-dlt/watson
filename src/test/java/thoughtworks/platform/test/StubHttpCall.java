package thoughtworks.platform.test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static thoughtworks.platform.test.StubHttpCall.StubHttpMethod.GET;

public class StubHttpCall {

    private StubHttpServletRequest request = new StubHttpServletRequest();
    private StubHttpServletResponseBuilder responseBuilder;

    public static StubHttpCall stubHttpRequest() {
        return new StubHttpCall();
    }

    public StubHttpCall() {

    }

    public StubHttpServletRequest servletRequest() {
        return request;
    }

    public StubHttpServletResponse servletResponse() {
        return responseBuilder.servletResponse();
    }

    public StubHttpCall withPath(String path) {
        request.path = path;
        return this;
    }


    public StubHttpCall GET(String path) {
        withPath(path);
        withMethod(GET);
        return this;
    }

    private StubHttpCall withMethod(StubHttpMethod method) {
        request.method = method;
        return this;
    }

    public StubHttpServletResponseBuilder returns() {
        this.responseBuilder = new StubHttpServletResponseBuilder(this);
        return responseBuilder;
    }

    public StubHttpCall withQueryString(String queryString) {
        request.queryString = queryString;
        return this;
    }

    enum StubHttpMethod {
        GET()


    }

}

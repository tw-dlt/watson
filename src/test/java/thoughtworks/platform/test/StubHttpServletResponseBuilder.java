package thoughtworks.platform.test;

import javax.servlet.http.HttpServletResponse;

public class StubHttpServletResponseBuilder {
    private StubHttpCall parent;
    private StubHttpServletResponse stubHttpServletResponse = new StubHttpServletResponse();

    public StubHttpServletResponseBuilder(StubHttpCall parent) {
        this.parent = parent;
    }

    public StubHttpServletResponseBuilder Ok() {
        stubHttpServletResponse.status = 200;
        return this;
    }

    public StubHttpCall build() {
        return parent;
    }

    public StubHttpServletResponse servletResponse() {
        return stubHttpServletResponse;
    }
}

package thoughtworks.platform.test;

import io.javalin.http.Context;
import thoughtworks.dlt.watson.JavalinContext;
import thoughtworks.dlt.watson.quorum.QuorumApiCache;
import thoughtworks.dlt.watson.quorum.StubQuorumApi;

import java.util.HashMap;
import java.util.Map;

public class StubJavalinContext extends Context {


    public static StubJavalinContextBuilder stubJavalinContext() {
        return new StubJavalinContextBuilder();
    }


    public StubJavalinContext(StubHttpServletRequest stubRequest, StubHttpServletResponse stubResponse, Map<Class<?>, Object> attributes) {
        super(stubRequest, stubResponse, attributes);
    }

    public static class StubJavalinContextBuilder {

        private StubHttpServletRequest stubRequest = new StubHttpServletRequest();
        private final StubHttpServletResponse stubResponse = new StubHttpServletResponse();
        private StubQuorumApi stubQuorumApi = new StubQuorumApi();


        public StubJavalinContextBuilder() {

        }

        public StubJavalinContext build() {
            StubJavalinContext ctx = new StubJavalinContext(stubRequest, stubResponse, new HashMap<>());
            JavalinContext.setQuorumApiCacheTo(ctx, new QuorumApiCache(stubQuorumApi));
            return ctx;
        }


        public StubJavalinContextBuilder withRequest(StubHttpServletRequest stubRequest) {
            this.stubRequest = stubRequest;
            return this;
        }

        public StubJavalinContextBuilder withQuorumApi(StubQuorumApi quorumApi) {
            this.stubQuorumApi = quorumApi;
            return this;
        }
    }
}

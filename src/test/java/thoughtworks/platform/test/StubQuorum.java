package thoughtworks.platform.test;

import io.reactivex.Flowable;
import org.jetbrains.annotations.NotNull;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.request.ShhFilter;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.*;
import org.web3j.protocol.websocket.events.LogNotification;
import org.web3j.protocol.websocket.events.NewHeadsNotification;
import org.web3j.quorum.Quorum;
import org.web3j.quorum.methods.request.PrivateTransaction;
import org.web3j.quorum.methods.response.PrivatePayload;
import thoughtworks.dlt.watson.quorum.StubGethRequest;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

public class StubQuorum implements Quorum {
    private EthBlockNumber ethBlockNumber;


    public void isAtBlockNumber(long blockNumber) {
        this.ethBlockNumber = new EthBlockNumber();
        this.ethBlockNumber.setResult("0x" + new BigInteger("" + blockNumber, 10).toString(16));
    }

    @Override
    public Request<?, EthSendTransaction> ethSendTransaction(PrivateTransaction transaction) {
        return null;
    }

    @Override
    public Request<?, PrivatePayload> quorumGetPrivatePayload(String hexDigest) {
        return null;
    }

    @Override
    public Request<?, Web3ClientVersion> web3ClientVersion() {
        return null;
    }

    @Override
    public Request<?, Web3Sha3> web3Sha3(String data) {
        return null;
    }

    @Override
    public Request<?, NetVersion> netVersion() {
        return null;
    }

    @Override
    public Request<?, NetListening> netListening() {
        return null;
    }

    @Override
    public Request<?, NetPeerCount> netPeerCount() {
        return null;
    }

    @Override
    public Request<?, EthProtocolVersion> ethProtocolVersion() {
        return null;
    }

    @Override
    public Request<?, EthCoinbase> ethCoinbase() {
        return null;
    }

    @Override
    public Request<?, EthSyncing> ethSyncing() {
        return null;
    }

    @Override
    public Request<?, EthMining> ethMining() {
        return null;
    }

    @Override
    public Request<?, EthHashrate> ethHashrate() {
        return null;
    }

    @Override
    public Request<?, EthGasPrice> ethGasPrice() {
        return null;
    }

    @Override
    public Request<?, EthAccounts> ethAccounts() {
        return null;
    }

    @Override
    public Request<?, EthBlockNumber> ethBlockNumber() {
        return new StubGethRequest<EthBlockNumber>(ethBlockNumber);
    }

    @Override
    public Request<?, EthGetBalance> ethGetBalance(String address, DefaultBlockParameter defaultBlockParameter) {
        return null;
    }

    @Override
    public Request<?, EthGetStorageAt> ethGetStorageAt(String address, BigInteger position, DefaultBlockParameter defaultBlockParameter) {
        return null;
    }

    @Override
    public Request<?, EthGetTransactionCount> ethGetTransactionCount(String address, DefaultBlockParameter defaultBlockParameter) {
        return null;
    }

    @Override
    public Request<?, EthGetBlockTransactionCountByHash> ethGetBlockTransactionCountByHash(String blockHash) {
        return null;
    }

    @Override
    public Request<?, EthGetBlockTransactionCountByNumber> ethGetBlockTransactionCountByNumber(DefaultBlockParameter defaultBlockParameter) {
        return null;
    }

    @Override
    public Request<?, EthGetUncleCountByBlockHash> ethGetUncleCountByBlockHash(String blockHash) {
        return null;
    }

    @Override
    public Request<?, EthGetUncleCountByBlockNumber> ethGetUncleCountByBlockNumber(DefaultBlockParameter defaultBlockParameter) {
        return null;
    }

    @Override
    public Request<?, EthGetCode> ethGetCode(String address, DefaultBlockParameter defaultBlockParameter) {
        return null;
    }

    @Override
    public Request<?, EthSign> ethSign(String address, String sha3HashOfDataToSign) {
        return null;
    }

    @Override
    public Request<?, EthSendTransaction> ethSendTransaction(Transaction transaction) {
        return null;
    }

    @Override
    public Request<?, EthSendTransaction> ethSendRawTransaction(String signedTransactionData) {
        return null;
    }

    @Override
    public Request<?, EthCall> ethCall(Transaction transaction, DefaultBlockParameter defaultBlockParameter) {
        return null;
    }

    @Override
    public Request<?, EthEstimateGas> ethEstimateGas(Transaction transaction) {
        return null;
    }

    @Override
    public Request<?, EthBlock> ethGetBlockByHash(String blockHash, boolean returnFullTransactionObjects) {
        return null;
    }

    @Override
    public Request<?, EthBlock> ethGetBlockByNumber(DefaultBlockParameter defaultBlockParameter, boolean returnFullTransactionObjects) {
        EthBlock block = new EthBlock();
        block.setResult(createBlock(defaultBlockParameter.getValue()));
        return new StubGethRequest<EthBlock>(block);
    }

    @NotNull
    private EthBlock.Block createBlock(String blockNumber) {
        String hash = "foobaaa" + blockNumber;
        return new EthBlock.Block(blockNumber, hash, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    @Override
    public Request<?, EthTransaction> ethGetTransactionByHash(String transactionHash) {
        return null;
    }

    @Override
    public Request<?, EthTransaction> ethGetTransactionByBlockHashAndIndex(String blockHash, BigInteger transactionIndex) {
        return null;
    }

    @Override
    public Request<?, EthTransaction> ethGetTransactionByBlockNumberAndIndex(DefaultBlockParameter defaultBlockParameter, BigInteger transactionIndex) {
        return null;
    }

    @Override
    public Request<?, EthGetTransactionReceipt> ethGetTransactionReceipt(String transactionHash) {
        return null;
    }

    @Override
    public Request<?, EthBlock> ethGetUncleByBlockHashAndIndex(String blockHash, BigInteger transactionIndex) {
        return null;
    }

    @Override
    public Request<?, EthBlock> ethGetUncleByBlockNumberAndIndex(DefaultBlockParameter defaultBlockParameter, BigInteger transactionIndex) {
        return null;
    }

    @Override
    public Request<?, EthGetCompilers> ethGetCompilers() {
        return null;
    }

    @Override
    public Request<?, EthCompileLLL> ethCompileLLL(String sourceCode) {
        return null;
    }

    @Override
    public Request<?, EthCompileSolidity> ethCompileSolidity(String sourceCode) {
        return null;
    }

    @Override
    public Request<?, EthCompileSerpent> ethCompileSerpent(String sourceCode) {
        return null;
    }

    @Override
    public Request<?, EthFilter> ethNewFilter(org.web3j.protocol.core.methods.request.EthFilter ethFilter) {
        return null;
    }

    @Override
    public Request<?, EthFilter> ethNewBlockFilter() {
        return null;
    }

    @Override
    public Request<?, EthFilter> ethNewPendingTransactionFilter() {
        return null;
    }

    @Override
    public Request<?, EthUninstallFilter> ethUninstallFilter(BigInteger filterId) {
        return null;
    }

    @Override
    public Request<?, EthLog> ethGetFilterChanges(BigInteger filterId) {
        return null;
    }

    @Override
    public Request<?, EthLog> ethGetFilterLogs(BigInteger filterId) {
        return null;
    }

    @Override
    public Request<?, EthLog> ethGetLogs(org.web3j.protocol.core.methods.request.EthFilter ethFilter) {
        return null;
    }

    @Override
    public Request<?, EthGetWork> ethGetWork() {
        return null;
    }

    @Override
    public Request<?, EthSubmitWork> ethSubmitWork(String nonce, String headerPowHash, String mixDigest) {
        return null;
    }

    @Override
    public Request<?, EthSubmitHashrate> ethSubmitHashrate(String hashrate, String clientId) {
        return null;
    }

    @Override
    public Request<?, DbPutString> dbPutString(String databaseName, String keyName, String stringToStore) {
        return null;
    }

    @Override
    public Request<?, DbGetString> dbGetString(String databaseName, String keyName) {
        return null;
    }

    @Override
    public Request<?, DbPutHex> dbPutHex(String databaseName, String keyName, String dataToStore) {
        return null;
    }

    @Override
    public Request<?, DbGetHex> dbGetHex(String databaseName, String keyName) {
        return null;
    }

    @Override
    public Request<?, ShhPost> shhPost(org.web3j.protocol.core.methods.request.ShhPost shhPost) {
        return null;
    }

    @Override
    public Request<?, ShhVersion> shhVersion() {
        return null;
    }

    @Override
    public Request<?, ShhNewIdentity> shhNewIdentity() {
        return null;
    }

    @Override
    public Request<?, ShhHasIdentity> shhHasIdentity(String identityAddress) {
        return null;
    }

    @Override
    public Request<?, ShhNewGroup> shhNewGroup() {
        return null;
    }

    @Override
    public Request<?, ShhAddToGroup> shhAddToGroup(String identityAddress) {
        return null;
    }

    @Override
    public Request<?, ShhNewFilter> shhNewFilter(ShhFilter shhFilter) {
        return null;
    }

    @Override
    public Request<?, ShhUninstallFilter> shhUninstallFilter(BigInteger filterId) {
        return null;
    }

    @Override
    public Request<?, ShhMessages> shhGetFilterChanges(BigInteger filterId) {
        return null;
    }

    @Override
    public Request<?, ShhMessages> shhGetMessages(BigInteger filterId) {
        return null;
    }

    @Override
    public Request<?, EthSendTransaction> ethSendRawPrivateTransaction(String signedTransactionData, List<String> privateFor) {
        return null;
    }

    @Override
    public void shutdown() {

    }

    @Override
    public Flowable<Log> ethLogFlowable(org.web3j.protocol.core.methods.request.EthFilter ethFilter) {
        return null;
    }

    @Override
    public Flowable<String> ethBlockHashFlowable() {
        return null;
    }

    @Override
    public Flowable<String> ethPendingTransactionHashFlowable() {
        return null;
    }

    @Override
    public Flowable<org.web3j.protocol.core.methods.response.Transaction> transactionFlowable() {
        return null;
    }

    @Override
    public Flowable<org.web3j.protocol.core.methods.response.Transaction> pendingTransactionFlowable() {
        return null;
    }

    @Override
    public Flowable<EthBlock> blockFlowable(boolean fullTransactionObjects) {
        return null;
    }

    @Override
    public Flowable<EthBlock> replayPastBlocksFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock, boolean fullTransactionObjects) {
        return null;
    }

    @Override
    public Flowable<EthBlock> replayPastBlocksFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock, boolean fullTransactionObjects, boolean ascending) {
        return null;
    }

    @Override
    public Flowable<EthBlock> replayPastBlocksFlowable(DefaultBlockParameter startBlock, boolean fullTransactionObjects, Flowable<EthBlock> onCompleteFlowable) {
        return null;
    }

    @Override
    public Flowable<EthBlock> replayPastBlocksFlowable(DefaultBlockParameter startBlock, boolean fullTransactionObjects) {
        return null;
    }

    @Override
    public Flowable<org.web3j.protocol.core.methods.response.Transaction> replayPastTransactionsFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        return null;
    }

    @Override
    public Flowable<org.web3j.protocol.core.methods.response.Transaction> replayPastTransactionsFlowable(DefaultBlockParameter startBlock) {
        return null;
    }

    @Override
    public Flowable<EthBlock> replayPastAndFutureBlocksFlowable(DefaultBlockParameter startBlock, boolean fullTransactionObjects) {
        return null;
    }

    @Override
    public Flowable<org.web3j.protocol.core.methods.response.Transaction> replayPastAndFutureTransactionsFlowable(DefaultBlockParameter startBlock) {
        return null;
    }

    @Override
    public Flowable<NewHeadsNotification> newHeadsNotifications() {
        return null;
    }

    @Override
    public Flowable<LogNotification> logsNotifications(List<String> addresses, List<String> topics) {
        return null;
    }
}
